FROM centos:7.8.2003

LABEL maintainer="John Sundh" email=john.sundh@nbis.se

# Use bash shell
SHELL ["/bin/bash", "-c"]

# Set workdir
WORKDIR /analysis

# Update packages
RUN yum -y update && yum -y install bzip2 git unzip && yum clean all

# Add conda to PATH and set locale
ENV PATH="/opt/miniconda3/bin:${PATH}"
# Add conda environment to path
ENV PATH="/opt/miniconda3/envs/workflow/bin:${PATH}"
ENV LC_ALL en_US.UTF-8
ENV LC_LANG en_US.UTF-8

RUN curl https://repo.anaconda.com/miniconda/Miniconda3-py37_4.8.3-Linux-x86_64.sh -O && \
   bash Miniconda3-py37_4.8.3-Linux-x86_64.sh -bf -p /opt/miniconda3/ && \
   rm Miniconda3-py37_4.8.3-Linux-x86_64.sh && \
   ln -s /opt/miniconda3/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
   /opt/miniconda3/bin/conda clean -tipsy && \
   echo ". /opt/miniconda3/etc/profile.d/conda.sh " >> ~/.bashrc && \
   echo "conda activate workflow" >> ~/.bashrc

# Set TMPDIR variable
ENV TMPDIR="/tmp"

# Add environment file
COPY environment.yaml .

# Install environment
RUN conda env create -f environment.yaml -n workflow && conda clean -tipsy

# Add workflow
RUN mkdir -p config envs samples
COPY config/* ./config/
COPY source ./source
COPY samples/all_metagenomes_best_assemblies.tab ./samples/
COPY envs/*.yaml ./envs/
COPY Snakefile .

CMD ["bin/bash"]