# A workflow for metagenomic projects

[TOC]

## Overview
This is a [snakemake](http://snakemake.readthedocs.io/en/stable/) workflow that 
processes paired-end and/or single-end metagenomic samples.

This is a 'legacy' version of the workflow hosted here for reproducibility 
purposes. The most up-to-date version of the workflow is currently found at
GitHub: [nbisweden/nbis-meta](https://github.com/nbisweden/nbis-meta).

## Installation
### Clone the repository
Checkout the latest version of this repository (to your current directory):

```
git clone git@bitbucket.org:scilifelab-lts/nbis-meta-legacy.git
```

### Install the required software
All the software needed to run this workflow is included as a
 [conda](http://anaconda.org) environment file. To create the environment 
 `sm-meta` use the supplied [environment.yaml](environment.yaml) file.

```
conda env create -f environment.yaml
```

Activate the environment using:

```
conda activate sm-meta
```

## Obtaining data